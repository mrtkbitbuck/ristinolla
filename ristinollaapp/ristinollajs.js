"use strict"

//pelilauta
//0=tyhjä, 1=1-pelaajan merkki, 2=2-pelaajan merkki
var board =[[0,0,0],
            [0,0,0],
            [0,0,0],    
        ]

//muuttuja pelaajan vuorolle 1 tai 2
var turn = 1;

//1=peli vielä kesken
var gameon =1;


var tarkista =function(lauta){
    
    //vaakavoitto
    for(var i=0;i<3;i++){
        for(var j=1;j<3;j++){
            if (lauta[i].filter(function(val){return val==j;}).length===3){
                return 1;
            }
        }
    }

    //pystyvoitto
    for(var i=0;i<3;i++){
        for(var k=1;k<3;k++){
            if ([lauta[0][i],lauta[1][i],lauta[2][i]].filter(
                        function(val){return val==k;}).length===3){
                return 1;
            }
        }
    }

    //ristiin voitto
    for(var k=1;k<3;k++){
        if ([lauta[0][0],lauta[1][1],lauta[2][2]].filter(
                    function(val){return val==k;}).length===3){
            return 1;
        }
        if ([lauta[2][0],lauta[1][1],lauta[0][2]].filter(
                    function(val){return val==k;}).length===3){
            return 1;
        }
    }
    
    //tasapeli tulee kun kaikilla pelilaudan paikoilla on jokin merkki
    var count=0;
    for(var i=0;i<3;i++){
        for (var j=0;j<3;j++){
            if(lauta[i][j]===1||lauta[i][j]===2){
                count+=1;
            }
        }
    }
    if(count==9){
        return 2;
    }
}


var siirto = function(y,x, lauta, vuoro){

    //tarkastetaan onko paikalla jo merkki
    //palautetaan -1 jos on jo
    if(lauta[y][x]===1||lauta[y][x]===2){
        //console.log(y.toString()+"."+x.toString()+ " ON JO!");
        return -1;
    }
    else{
        //tehdään siirto laudan tietorakenteeseen
        lauta[y][x]=vuoro;
        return 1;
    }
}

function clicked(y,x){
    if(gameon == 1){
        var nykyinenvuoro = turn;
        var seuraavavuoro = (turn==1? 2:1);
        
        //jos siirto oli hyväksyttävä, päivitetään html:ää ja vaihdetaan vuoroa
        if(siirto(y,x, board, nykyinenvuoro)===-1){
        }else{
            
            var p = document.getElementById(y.toString()+x.toString());
            var v = document.getElementById("turnbox");

            p.textContent=(nykyinenvuoro==1? "X":"O");
            v.textContent="Pelaajan " + (seuraavavuoro==1? "X":"O") +" vuoro!";

            turn = (turn==1? 2:1);

        }

        //tarkistetaan onko voittoa tai tasapeliä
        var tarkistus = tarkista(board);
        if(tarkistus===1){
            v.textContent="Pelaaja " + (nykyinenvuoro==1? "X":"O") + " voitti!";
            gameon=0;
        }
        if(tarkistus===2){
            v.textContent="Tasapeli!";
            gameon=0;
        }
    }

}